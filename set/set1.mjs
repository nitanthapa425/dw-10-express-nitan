// [1,2,1]  = [1,2]   // remove duplicate array

// let s1 = new Set([1, 2, 1]); // {1,2}
// let uniqueValue = [...s1]; // 1,2
// console.log(uniqueValue);

let input = [1, 2, 3, 1, 1];

let uniqueValue = [...new Set(input)];

console.log(uniqueValue);
