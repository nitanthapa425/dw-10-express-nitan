// console.log(JSON.parse("1")); //1
// console.log(JSON.parse("[1,2,3]")); //[1,2,3]

// [1,2,[1,3],[1,3]]  ======= [1,2,[1,3]]

// map

// [1,2,[1,3],[1,3]]   ===== ["1","2","[1,3]","[1,3]"]

// set
// ["1","2","[1,3]","[1,3]"] === ["1","2","[1,3]"]

// parse
// ["1","2","[1,3]"]====[1,2,[1,3]]

let input = [1, 2, [1, 3], [1, 3]];

let stringyfiyAr = input.map((value, i) => {
  return JSON.stringify(value);
});

let uniqueValue = [...new Set(stringyfiyAr)]; //["1","2","[1,3"]

let desiredOutput = uniqueValue.map((value, i) => {
  return JSON.parse(value);
});

console.log(desiredOutput);
