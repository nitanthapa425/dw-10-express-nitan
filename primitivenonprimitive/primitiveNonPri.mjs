// let a = 1;
// let b = a;
// a = 5;
// console.log(a); //5
// console.log(b); //1

// let ar1 = [1, 2];
// let ar2 = ar1;
// ar2.push(3);
// console.log(ar1); //[1,2,3]
// console.log(ar2); //[1,2,3]

// console.log([1, 2] === [1, 2]);

//primitive
//string, boolean, number, undefined, null

//non primitive
//obj, array

//memory allocation in primitive
// if let is used => memory wil allocate for that variable
// let a = 1;
// let b = a;
// a = 5;
// console.log(a);
// console.log(b);

//memory allocation in non primitive

// let ar1 = [1, 2];
// let ar2 = ar1;
// ar1.push(3);
// console.log(ar1);
// console.log(ar2);

// ===
//primitive
let a = 1;
let b = 1;
let c = a;
console.log(a === b); //true
console.log(a === c); //true

//non primitive
let x = [1, 2];
let y = x;

console.log(x === y); //true

// console.log([1, 2] === [1, 2]); //false
