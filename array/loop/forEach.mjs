let ar = ["a", "b", "c"];

ar.forEach((value, i) => {
  console.log(value);
  console.log(i);
  console.log("hello");
});

ar.forEach((value, i) => {
  console.log(`${value}${i}`);
});

let ar3 = ["nitan", "ram", "hari"];

ar3.forEach((value, i) => {
  console.log(`my name is ${value} `);
});

// my name is nitan
// my name is ram
// my name is hari
