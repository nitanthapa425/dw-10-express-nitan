let ar1 = [1, 2, 3];
//                     3    3
let value = ar1.reduce((pre, cur) => {
  return pre + cur;
}, 0); //1//3//6

console.log(value);
