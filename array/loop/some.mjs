// [1,2,3]  has 3 in the list

let ar1 = [1, 2, 3];

//if one of the element return true then the output is true
let has3 = ar1.some((value, i) => {
  if (value === 3) {
    return true;
  } else {
    return false;
  }
});
