// this (operator)
// this operator are those which point itself
//dif between arrow function and normal function

let info = {
  name: "nitan",
  surname: "thapa",
  age: 29,
  fullDetails: function () {
    console.log(`my name is ${this.name} ${this.surname}. i am ${this.age}`);
  },
  infoinfo: {
    info1: { address: "gagal" },
    favFood: ["apple", "banana"],
  },
};

// console.log(info.name);
// info.fullDetails();

// console.log(info.infoinfo.info1.address)
