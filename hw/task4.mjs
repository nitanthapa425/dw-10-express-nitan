//nitAn => Nitan

//         split("")                                        join("")
// "nitAn" => ["n","i","t","A","n"] =>["N","i","t","a","n"] => "Nitan"

let input = "nitAn";

let inputAr1 = input.split(""); //["n","i","t","A","n"]

let outputAr1 = inputAr1.map((value, i) => {
  if (i === 0) {
    return value.toUpperCase();
  } else {
    return value.toLowerCase();
  }
});

let output = outputAr1.join("");
console.log(output);
